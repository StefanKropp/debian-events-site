Title: Demo Test Event 2 - A BoF Session
Date: 2024-10-01 10:20
Modified: 2024-10-02 10:30
Category: DebBoF
Tags: online, bof
Slug: Demo/demo-test-2
Authors: Stefan Kropp
Summary: XMPP Team BOF 
Eventexpire: 20241205
Eventdate: Somewhen
Eventlocation: Somewhere at the web
Eventwiki: https://wiki.debian.org
EventOSM: https://www.openstreetmap.org/?mlat=0.428&mlon=50.576#map=8/0.428/50.576
EventStatus: preparation
EventLanguage: Lojban

This is a online session about XMPP. 
