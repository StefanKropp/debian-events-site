Title: Demo Test Event - Online
Date: 2024-10-02 10:20
Modified: 2024-10-02 10:30
Category: DebWorkshop
Tags: online, workshop
Slug: Demo/demo-test-1
Authors: Stefan Kropp
Summary: Chatting with XMPP 
Eventexpire: 20241205
Eventdate: Somewhen
Eventlocation: Somewhere at the web
Eventwiki: https://wiki.debian.org
EventOSM: https://www.openstreetmap.org/?mlat=0.428&mlon=50.576#map=8/0.428/50.576
EventStatus: preparation
EventLanguage: Lojban

This is a online session about XMPP. 
