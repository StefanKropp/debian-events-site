Title: About Debian
Date: 2024-05-28 17:30
Modified: 2024-05-28 17:30
Slug: about-debian
Authors: Stefan Kropp

Debian is a project to provide a free (libre) operating system.
[Debian Homepage](https://www.debian.org)
