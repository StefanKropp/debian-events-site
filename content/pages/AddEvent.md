Title: Add event 
Date: 2024-02-02 10:20
Modified: 2024-02-02 10:30
Slug: addevent 
Authors: Stefan Kropp 

# Create a new event

```
Title: Title of the event 
Date: 2024-03-02 10:20
Modified: 2024-03-02 10:30
Category: Online
Tags: online
Slug: Online/2024-03-02-Test4
Authors: Stefan Kropp
Summary: A online event
Eventexpire: 20240305
Eventdate: Somewhen
Eventlocation: Somewhere at the web
Eventwiki: https://wiki.debian.org
EventOSM: https://www.openstreetmap.org/?mlat=0.428&mlon=50.576#map=8/0.428/50.576
EventStatus: preparation
EventLanguage: Lojban

This is the content of my super blog post.
```

# Edit the event
 
 * Date: First date of the event 
 * Category: Online, DebConf, MiniDebConf, LocalGroup
 * Authors: Contact
 * EventStatus: preparation or confirmed

