Title: DebConf24
Date: 2024-07-28 00:00
Modified: 2024-02-02 10:30
Category: DebConf
Tags: debconf, asia, southkorea, english
Slug: debconf/DebConf24 
Authors: Debian
Summary: The 25th Debian Conference is in Busan, South Korea 
Eventexpire: 20240805
Eventdate: Sunday July 28th to Sunday August 4th 2024 
Eventlocation: 
Eventwiki: 
EventOSM: 
EventStatus: preparation
EventLanguage: English

<p>
The 25th Debian Conference is in Busan, South Korea, Sunday July 28th to Sunday August 4th 2024. DebCamp will be held
from Sunday July 21st to Saturday July 27th 2024. 
</p>

