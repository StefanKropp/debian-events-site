Title: DebCamp @ DebConf24
Date: 2024-07-21 00:00
Modified: 2024-02-02 10:30
Category: DebCamp
Tags: debconf, asia, southkorea, english
Slug: debconf/DebCamp24
Authors: Debian
Summary: DebCamp at DebConf24 in South Korea
Eventexpire: 20240805
Eventdate: July 21st to Saturday July 27th 2024 
Eventlocation: 
Eventwiki: 
EventOSM: 
EventStatus: preparation
EventLanguage: English

<p>
DebCamp will be held from Sunday July 21st to Saturday July 27th 2024.
</p>

