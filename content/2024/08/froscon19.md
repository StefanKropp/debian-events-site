Title: FrOSCon19 
Date: 2024-08-17 00:00
Modified: 2024-02-02 10:30
Category: DebVisiting
Tags: germany, german
Slug: 2024/08/froscon19
Authors: Stefan Kropp
Summary: Free and Open Source Software Conference 
Eventexpire: 20240818
Eventdate: 17. + 18. August 2024 
Eventlocation: D-53757 Sankt Augustin
Eventwiki: https://wiki.debian.org/DebianEvents/de/2024/FrOSCon19
EventOSM: https://www.openstreetmap.org/?minlon=7.18247890472412&minlat=50.7794532775879&maxlon=7.18388938903809&maxlat=50.7800941467285#map=19/50.77977/7.18318
EventStatus:
EventLanguage: German, English

Auf der [FrOSCon](https://froscon.org/) erwartet den Besucher
verschiedene Vorträgen und Workshops zum Thema Free und Open Source Software.
Veranstaltet wird die Konferenz vom Fachbereich Informatik der
Hochschule Bonn-Rhein-Sieg mit Hilfe des FrOSCon e.V.

## Besucher

 * [Stefan Kropp](https://wiki.debian.org/StefanKropp)
