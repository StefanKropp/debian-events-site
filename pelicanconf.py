AUTHOR = 'Stefan Kropp'
SITENAME = 'Debian Events'
#SITEURL = 'https://stefankropp.pages.debian.net/debian-events-site'

PATH = 'content'

TIMEZONE = 'Europe/Berlin'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = 'feeds/all.atom.xml'
FEED_ALL_RSS = 'feeds/all.rss.xml'
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://www.python.org/'),
         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = 10

CSS_FILE = 'main.css'
ARTICLE_ORDER_BY = 'date'

THEME = './themes/simple/'
STATIC_PATHS = ["static", "images"]
PATH = 'content'

DEB_EVENTS_REPOSITORY='https://salsa.debian.org/StefanKropp/debian-events-site'
DEB_EVENTS_ISSUES='https://salsa.debian.org/StefanKropp/debian-events-site/-/issues'
DEB_EVENTS_HELP='https://salsa.debian.org/StefanKropp/debian-events-site/-/wikis/home'

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
